﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System;
using System.Collections.Generic;
using AlleyLabs.AutoBuild;
using System.IO;
using System.Linq;
using AlleyLabs;

public static class BuildCommand
{
	private static string[] GetBuildScenes()
	{
		List<string> names = new List<string>();
		
		foreach (EditorBuildSettingsScene e in EditorBuildSettings.scenes)
		{
			if (e == null)
			{
				continue;
			}
			
			if (e.enabled)
			{
				names.Add(e.path);
			}
		}
		
		return names.ToArray();
	}



	public static void BuildIOSCommand(BuildSettings settings, bool development)
	{
		string[] scenes = GetBuildScenes();
		string path = GetBuildPath("iPhone");
		if (scenes == null || scenes.Length == 0 || path == null)
		{
			return;
		}

		ApplyIOSBuildSettings(settings, development);
		BuildPipeline.BuildPlayer(scenes, path, BuildTarget.iOS, BuildOptions.None);
	}

	private static void BuildIOSDevelopment()
	{
		BuildIOSCommand(BuildSettings.Instance, true);
	}

	private static void BuildIOSProduction()
	{
		BuildIOSCommand(BuildSettings.Instance, false);
		AntRun.RunAntTarget("export-ipa");
	}

	[MenuItem("Alley Labs/Build/iOS Production")]
	public static void LocalIOSBuildProduction()
	{
		AntRun.RunAntTarget("clean");
		BuildIOSCommand(BuildSettings.Instance, false);
		AntRun.RunAntTarget("export-ipa-production");
	}

	[MenuItem("Alley Labs/Build/iOS Development")]
	public static void LocalIOSBuildDevelopment()
	{
		AntRun.RunAntTarget("clean");
		BuildIOSCommand(BuildSettings.Instance, true);
		AntRun.RunAntTarget("export-ipa-development");
	}

	private static void ApplyCommonSettingsBetweenIOSAndAndroid()
	{
		
	}

	private static void ApplyIOSBuildSettings(BuildSettings buildSettings, bool development = true, bool localBuild = false)
	{
		var iOSVersion = development ? buildSettings.developmentVersion.iOSVersion : buildSettings.productionVersion.iOSVersion;
		string buildFlags = development ? buildSettings.developmentVersion.buildFlag.ToString() : buildSettings.productionVersion.buildFlag.ToString();
		var iOSSettings = buildSettings.settings.iOSBuildSettings;
		PlayerSettings.iOS.targetDevice = iOSSettings.targetDevice;
		PlayerSettings.iOS.targetOSVersion = iOSSettings.targetiOSVersion;


		PlayerSettings.iOS.buildNumber = iOSVersion.bundleCode.ToString();
		PlayerSettings.bundleVersion = iOSVersion.version;

		PlayerSettings.bundleIdentifier = buildSettings.commonSettings.gameBundle;
		PlayerSettings.companyName = buildSettings.commonSettings.companyName;
		PlayerSettings.productName = buildSettings.commonSettings.productName;

		PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.iOS, buildFlags + ";DOTWEEN_TK2D;NO_GPGS;TK2D;TextMeshPro");

		//Screen.SetResolution(
		//Default Settings
		//PlayerSettings.iOS.targetResolution = iOSTargetResolution.Native;

	}

	private static void ApplyAndroidBuildSettings(BuildSettings buildSettings, bool development = true, bool localBuild = false, AndroidTargetDevice deviceSupport = AndroidTargetDevice.FAT)
	{
		var androidVersion = development ? buildSettings.developmentVersion.androidVersion : buildSettings.productionVersion.androidVersion;
		var androidSettings = buildSettings.settings.androidBuilSettings;
		string buildFlags = development ? buildSettings.developmentVersion.buildFlag.ToString() : buildSettings.productionVersion.buildFlag.ToString();

		PlayerSettings.Android.keystoreName = FindFullFilePath(androidSettings.keystore);
		PlayerSettings.Android.keystorePass = androidSettings.keystorePass;
		PlayerSettings.Android.keyaliasName = androidSettings.keyaliasName;
		PlayerSettings.Android.keyaliasPass = androidSettings.keyaliasPass;
		PlayerSettings.bundleVersion = androidVersion.version;
		PlayerSettings.Android.targetDevice = deviceSupport;
		PlayerSettings.bundleIdentifier = buildSettings.commonSettings.gameBundle;
		PlayerSettings.companyName = buildSettings.commonSettings.companyName;
		PlayerSettings.productName = buildSettings.commonSettings.productName;

		PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android, buildFlags + ";DOTWEEN_TK2D;TK2D;TextMeshPro");
		int prefixDeviceTarget = PlayerSettings.Android.targetDevice == AndroidTargetDevice.ARMv7 ? 1 : (PlayerSettings.Android.targetDevice == AndroidTargetDevice.x86 ? 2 : 3);

		PlayerSettings.Android.bundleVersionCode = androidVersion.bundleCode;

		if (!localBuild)
		{
			EditorPrefs.SetString("AndroidSdkRoot", buildSettings.commonSettings.androidSDK);
		}
	}


	private static int GenerateAndroidBundleVersionCodeForProduction(int shortBundleCode, string buildVersion)
	{
		// Bunlde code converion:
		// Architecture(1) + API(2) + BuildVersion(3) + BundleCode(2)= 8_15_110_22 = 8 digits

		int prefixDeviceTarget = 9; //for both armv7 and x86_64
		prefixDeviceTarget = PlayerSettings.Android.targetDevice == AndroidTargetDevice.ARMv7 ? 2 : (PlayerSettings.Android.targetDevice == AndroidTargetDevice.x86 ? 8 : 9);

		int plusCode = PlayerSettings.Android.targetDevice == AndroidTargetDevice.ARMv7 ? 1 : (PlayerSettings.Android.targetDevice == AndroidTargetDevice.x86 ? 2 : 3);

		int buildNumber = (int)(float.Parse(buildVersion) * 10000);

		int nextBundleCode = GetBundleCode() + 3;

		return 10000000 * 8 + 100000 * PlayerSettings.Android.minSdkVersion.GetHashCode() + buildNumber + (nextBundleCode + plusCode);
	}

	public static string FindFullFilePath(string fileName)
	{
		List<string> provisionFiles = Directory.GetFiles(Application.dataPath, fileName, SearchOption.AllDirectories).ToList();
		return provisionFiles[0];
	}

	private static void SetBuildeNumber()
	{
		Environment.SetEnvironmentVariable("BUILD_NUMBER", "1");
	}

	private static int GetBundleCode()
	{
		string build_number_string = Environment.GetEnvironmentVariable("BUILD_NUMBER");
		if (build_number_string != null)
		{
			try
			{
				int buildNumber = 1;
				Int32.TryParse(build_number_string, out buildNumber);
				if (buildNumber > 99)
				{
					SetBuildeNumber();
					buildNumber = 1;
				}
				return buildNumber;
			}
			catch (Exception ex)
			{
				Debugger.Log("Error: " + ex.Message);
				return 1;
			}
		}
		return 1;
	}

	private static string GetBuildPath(string target)
	{
		string dirPath = Application.dataPath + "/../Build/" + target;
		if (!System.IO.Directory.Exists(dirPath))
		{
			try
			{
				var dir = new DirectoryInfo(Application.dataPath + "/../Build/");
				dir.Delete(true);

				System.IO.Directory.CreateDirectory(dirPath);
			
			
			}
			catch (Exception ex)
			{
				System.IO.Directory.CreateDirectory(dirPath);
			}


		}
		else
		{
			var dir = new DirectoryInfo(Application.dataPath + "/../Build/");
			Debug.Log(dir.ToString());
			dir.Delete(true);
			System.IO.Directory.CreateDirectory(dirPath);
		}
		return dirPath;
	}

	private static void BuildAndroid(BuildSettings settings, bool development, bool localBuild, BuildOptions options = BuildOptions.None, AndroidTargetDevice deviceTarget = AndroidTargetDevice.FAT)
	{
		string[] scenes = GetBuildScenes();

		string path = GetBuildPath("android");

		if (scenes == null || scenes.Length == 0 || path == null)
		{
			return;
		}
		ApplyAndroidBuildSettings(settings, development, localBuild, deviceTarget);
		BuildPipeline.BuildPlayer(scenes, path, BuildTarget.Android, options);	
	}

	public static void BuildAndroidDevelopmentRemote()
	{
		BuildAndroid(BuildSettings.Instance, true, false);
	}

	private static void BuildAndroidProductionRemote()
	{
		//Command build
		BuildAndroid(BuildSettings.Instance, false, false);
	}

	private static void BuildAndroidProductionx86Remote()
	{
		//Command build
		BuildAndroid(BuildSettings.Instance, false, false, BuildOptions.None, AndroidTargetDevice.x86);
	}

	private static void BuildAndroidProductionARMv7emote()
	{
		//Command build
		BuildAndroid(BuildSettings.Instance, false, false, BuildOptions.None, AndroidTargetDevice.ARMv7);
	}

	[MenuItem("Alley Labs/Build/Android Development")]
	public static void LocalAndroidBuildDevelopment()
	{
		BuildAndroid(BuildSettings.Instance, true, true);
		AntRun.RunAntTarget("rename-android-build");
	}

	[MenuItem("Alley Labs/Android Build And Run %g")]
	public static void LocalAndroidAutoRun()
	{
		BuildAndroid(BuildSettings.Instance, true, true, BuildOptions.AutoRunPlayer);
		AntRun.RunAntTarget("rename-android-build");
	}

	[MenuItem("Alley Labs/Build/Android Production")]
	public static void LocalAndroidBuildProduction()
	{
		BuildAndroid(BuildSettings.Instance, false, true);
		AntRun.RunAntTarget("rename-android-build");
	}
	
}
